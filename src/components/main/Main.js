import { useState } from "react";
import { ListContainer } from "../listContainer/ListContainer"
import styles from "./Main.module.css";

export const Main = () => {

    const[unSeenPokemons,setunSeenPokemons] = useState(["Pikachu","Charizard","Eevee","Snorlax","Mewtwo","Squirtle","Ditto","Bulbasaur","Lucario","Gengar"]);
    const[seenPokemons,setSeenPokemons] = useState([]);

    const markPokemonSeen = (e) => {
        const pokemonName = e.target.id;
        const filteredPokemonList = unSeenPokemons.filter((name) => name !==pokemonName );
        setunSeenPokemons(filteredPokemonList);
        seenPokemons.push(pokemonName);
        setSeenPokemons(seenPokemons);
    } 

    const markPokemonUnseen = (e) =>{
        const pokemonName = e.target.id;
        const filteredPokemonList = seenPokemons.filter((name) => name !==pokemonName );
        setSeenPokemons(filteredPokemonList);
        unSeenPokemons.push(pokemonName);
        setunSeenPokemons(unSeenPokemons);
    }

    return( 
        <main className={styles.main}>
            <ListContainer title="List Of Seen Pokemon" pokemonList={seenPokemons} onClickHandler = {markPokemonUnseen}/>
            <ListContainer title="List Of Unseen Pokemon" pokemonList={unSeenPokemons} onClickHandler = {markPokemonSeen}/>
        </main>
    );
} 