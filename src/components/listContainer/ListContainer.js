import styles from './ListContainer.module.css';

export const ListContainer = ({title,pokemonList,onClickHandler}) => {
    return(
        <div className={styles.container}>
            <h2>{title}</h2>

            <ul>

                {
                    pokemonList.map((pokemonName) => 
                        <>
                            <li key = {pokemonName} onClick={e => onClickHandler(e)} id={pokemonName}>{pokemonName}</li>
                        </>
                    )
                }

            </ul>
        </div>
    );
}